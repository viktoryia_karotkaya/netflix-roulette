export interface Movie {
  id: string;
  name: string;
  urlName: string;
  releaseDate: number;
  description: string;
}
